migrations:
	.\vendor\bin\doctrine orm:schema-tool:create

migrations-update:
	.\vendor\bin\doctrine orm:schema-tool:update --force

migrations-drop:
	.\vendor\bin\doctrine orm:schema-tool:drop --force

