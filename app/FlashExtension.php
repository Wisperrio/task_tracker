<?php

namespace App;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('flash', [$this, 'flash']),
        ];
    }

    public function flash($name)
    {
        $session = Session::getInstance();
        $message = $session->$name;
        unset($session->$name);

        return $message;
    }
}