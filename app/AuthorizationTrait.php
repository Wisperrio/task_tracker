<?php

namespace App;

use App\Entity\User;

trait AuthorizationTrait
{

    protected function authUser(User $user)
    {
        setcookie('Authorization', $user->getId());
        setcookie('Login', $user->getLogin());
    }

    protected function logoutUser()
    {
        setcookie('Authorization', '', time() - 3600);
        setcookie('Login', '', time() - 3600);
    }

    /**
     * @return int
     * @throws \DomainException
     */
    protected function userLogin(): int
    {
        if (!$_COOKIE['Authorization']) {
            throw new \DomainException('Need auth');
        }

        return $_COOKIE['Authorization'];
    }
}