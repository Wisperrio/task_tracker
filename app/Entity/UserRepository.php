<?php

namespace App\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository
{
    private EntityManagerInterface $em;
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct(EntityManagerInterface $em, $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    public function hasByLogin($login): bool
    {
        return $this->repository->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.login = :login')
                ->setParameter(':login', $login)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function oneByLoginAndPassword($login, $password)
    {
        return $this->repository->findOneBy(['login' => $login, 'password' => $password]);
    }

    public function oneById($id)
    {
        return $this->repository->findOneBy(['id' => $id]);
    }

    public function add(User $user): void
    {
        $this->em->persist($user);
    }
}