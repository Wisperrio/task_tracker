<?php

namespace App\Entity;

use Assert\Assertion;
use Cake\Validation\Validation;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use DomainException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 */
class User
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    /** @Column(type="string", length=255, unique=true) */
    private $login;
    /** @Column(type="string") */
    private $password;
    /** @Column(type="auth_user_status", length=16) */
    private Status $status;
    /** @Column(type="auth_user_role", length=16) */
    private $role;
    /** @Column(type="datetime") */
    private $created_at;
    /** @Column(type="datetime") */
    private $updated_at;

    public function __construct(Status $status)
    {
        $this->status = $status;
        $this->role = Role::user();
    }

    public static function create(
        string $login,
        string $passwordHash
    ) : self {
        $user = new self(Status::active());
        $user->password = $passwordHash;
        $user->login = $login;

        return $user;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function isAdmin(): bool
    {
        return $this->role->isAdmin();
    }

    public function isUser(): bool
    {
        return $this->role->isUser();
    }

    public function isWait(): bool
    {
        return $this->status->isDeleted();
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }
}