<?php

declare(strict_types=1);

namespace App\Entity;

use Assert\Assertion;

class Role
{
    public const USER = 'user';
    public const ADMIN = 'admin';

    private string $name;

    public function __construct(string $name)
    {
        Assertion::inArray($name, [
            self::USER,
            self::ADMIN,
        ], 'Unknown role');

        $this->name = $name;
    }

    public static function user(): self
    {
        return new self(self::USER);
    }

    public function isAdmin(): bool
    {
        return $this->name === self::ADMIN;
    }

    public function isUser(): bool
    {
        return $this->name === self::USER;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
