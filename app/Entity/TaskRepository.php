<?php

namespace App\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class TaskRepository
{
    private EntityManagerInterface $em;
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct(EntityManagerInterface $em, $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    public function all($orderBy='id', $sortBy='ASC')
    {
        return $this->repository->createQueryBuilder('u')
                ->addOrderBy('u.' . $orderBy, $sortBy)
                ->getQuery();
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function oneById(int $id)
    {
        return $this->repository->findOneBy(['id' => $id]);
    }

    public function add(Task $task): void
    {
        $this->em->persist($task);
    }

    public function update(Task $task): void
    {
        $this->em->merge($task);
    }
}