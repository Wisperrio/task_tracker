<?php

declare(strict_types=1);

namespace App\Entity;

use Assert\Assertion;

class Status
{
    public const DELETED = 'deleted';
    public const ACTIVE = 'active';
    public const COMPLETE = 'complete';

    private string $name;

    public function __construct(string $name)
    {
        Assertion::inArray($name, [
            self::DELETED,
            self::ACTIVE,
            self::COMPLETE,
        ], 'Unknown status');

        $this->name = $name;
    }

    public static function delete(): self
    {
        return new self(self::DELETED);
    }

    public static function active(): self
    {
        return new self(self::ACTIVE);
    }

    public static function complete(): self
    {
        return new self(self::COMPLETE);
    }

    public function isDeleted(): bool
    {
        return $this->name === self::DELETED;
    }

    public function isActive(): bool
    {
        return $this->name === self::ACTIVE;
    }

    public function isCompleted(): bool
    {
        return $this->name === self::COMPLETE;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
