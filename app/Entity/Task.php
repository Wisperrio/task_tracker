<?php

namespace App\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use DomainException;

/**
 * @Entity
 * @Table(name="tasks")
 * @ORM\HasLifecycleCallbacks
 */
class Task
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;
    /** @Column(type="string", length=255, nullable=false) */
    private $title;
    /** @Column(type="string", length=255, nullable=false) */
    private $email;
    /** @Column(type="string", length=500, nullable=true) */
    private $description;
    /** @Column(type="auth_user_status", length=16) */
    private Status $status;
    /** @Column(type="datetime", nullable=false) */
    private $created_at;
    /** @Column(type="datetime", nullable=false) */
    private $updated_at;

    public function __construct(Status $status)
    {
        $this->status = $status;
    }

    public static function create($email, $title, $description = null) : self {
        Assertion::email($email);

        $task = new self(Status::active());
        $task->email = $email;
        $task->title = $title;
        $task->description = $description;

        return $task;
    }

    public function complete(): void
    {
        if ($this->isCompleted()) {
            throw new DomainException('Unable to complete competed task.');
        }

        $this->status = Status::complete();
    }

    public function isCompleted(): bool
    {
        return $this->status->isCompleted();
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    public function isUpdated(): bool
    {
        return $this->created_at < $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}
