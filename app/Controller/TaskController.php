<?php

namespace App\Controller;

use App\AuthorizationTrait;
use App\Entity\Task;
use App\Entity\TaskRepository;
use App\Entity\UserRepository;
use App\Flusher;
use App\Session;
use Assert\Assert;
use Assert\Assertion;
use Assert\AssertionFailedException;
use DomainException;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class TaskController
{
    use AuthorizationTrait;

    private Environment $view;
    private TaskRepository $tasks;
    private UserRepository $users;
    private Flusher $flusher;


    public function __construct(TaskRepository $tasks, Flusher $flusher, Environment $view, UserRepository $users)
    {
        $this->view = $view;
        $this->tasks = $tasks;
        $this->users = $users;
        $this->flusher = $flusher;
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $template = $this->view->load('tasks/index.html.twig');
        $orderBy = $_GET['orderBy'] ?? 'id';
        $count = $_GET['count'] ?? 3;

        if (strpos($orderBy, '-') === false) {
            $sortBy = 'ASC';
        } else {
            $sortBy = 'DESC';
        }

        $orderBy = str_replace('-', '', $orderBy);
        $tasks = $this->tasks->all($orderBy, $sortBy);
        $adapter = new QueryAdapter($tasks);
        $paginator = new Pagerfanta($adapter);
        $paginator->setMaxPerPage($count);
        $paginator->setCurrentPage($_GET['page'] ?? 1);

        return new HtmlResponse($template->render(['pager' => $paginator]));
    }

    public function create(ServerRequestInterface $request): ResponseInterface
    {
        $template = $this->view->load('tasks/create.html.twig');

        return new HtmlResponse($template->render());
    }

    public function store(ServerRequestInterface $request): ResponseInterface
    {
        $email = $_POST['email'] ?? null;
        $title = $_POST['title'] ?? null;
        $description = $_POST['description'] ?? null;

        try {
            Assert::that($email)->email()->notEmpty();
            Assertion::notEmpty($title);
            $task = Task::create($email, $title, $description);
            $this->tasks->add($task);
            $this->flusher->flush();
        } catch (AssertionFailedException $e) {
            Session::addError($e->getMessage());

            return new RedirectResponse('/tasks/create');
        }

        Session::addSuccess('Task ' . $task->getId() .' created');

        return new RedirectResponse('/tasks');
    }

    public function show(ServerRequestInterface $request): ResponseInterface
    {
        $template = $this->view->load('tasks/show.html.twig');
        /** @var Task $task */
        $task = $this->tasks->oneById($request->getAttribute('id'));

        if (!$task) {
            Session::addError('Task not found');

            return new RedirectResponse('/tasks');
        }

        return new HtmlResponse($template->render(compact('task')));
    }

    public function edit(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
            Session::addError('Need auth');

            return new RedirectResponse('/tasks');
        }

        /** @var Task $task */
        $task = $this->tasks->oneById($request->getAttribute('id'));

        if (!$task) {
            Session::addError('User already exists');

            return new RedirectResponse('/tasks');
        }

        $template = $this->view->load('tasks/update.html.twig');

        return new HtmlResponse($template->render(compact('task')));
    }

    public function update(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
            Session::addError('Need auth');

            return new RedirectResponse('/tasks');
        }

        $email = $_POST['email'] ?? null;
        $title = $_POST['title'] ?? null;
        $description = $_POST['description'] ?? null;

        /** @var Task $task */
        $task = $this->tasks->oneById($request->getAttribute('id'));

        if (!$task) {
            Session::addError('User already exists');

            return new RedirectResponse('/tasks');
        }

        if ($email) {
            Assertion::email($email);
            $task->setEmail($email);
        }

        if ($title) {
            $task->setTitle($title);
        }

        $task->setDescription($description);
        $this->tasks->update($task);
        $this->flusher->flush();

        return new RedirectResponse('/tasks');
    }

    public function complete(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
            Session::addError('Need auth');

            return new RedirectResponse('/tasks');
        }

        /** @var Task $task */
        $task = $this->tasks->oneById($request->getAttribute('id'));

        if (!$task) {
            Session::addError('User already exists');

            return new RedirectResponse('/tasks');
        }

        $task->complete();
        $this->tasks->update($task);
        $this->flusher->flush();

        return new RedirectResponse('/tasks');
    }
}