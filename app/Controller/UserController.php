<?php

namespace App\Controller;

use App\AuthorizationTrait;
use App\Entity\User;
use App\Entity\UserRepository;
use App\Flusher;
use App\Session;
use Assert\Assertion;
use DomainException;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class UserController
{
    use AuthorizationTrait;

    private Environment $view;
    private UserRepository $users;
    private Flusher $flusher;

    public function __construct(UserRepository $users, Flusher $flusher, Environment $view)
    {
        $this->view = $view;
        $this->users = $users;
        $this->flusher = $flusher;
    }

    public function loginForm(): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
        }

        $template = $this->view->load('users/login.html.twig');

        return new HtmlResponse($template->render());
    }

    public function login(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
        }

        $login = $_POST['login'] ?? null;
        $password = $_POST['password'] ?? null;
        Assertion::notEmpty($login);
        Assertion::notEmpty($password);

        /** @var User $user */
        $user = $this->users->oneByLoginAndPassword($login, md5($password));
        if (!$user) {
            Session::addError('Incorrect password');

            return new RedirectResponse('/login');
        }

        $this->authUser($user);

        return new RedirectResponse('/tasks');
    }

    public function logout(): ResponseInterface
    {
        $this->userLogin();
        $this->logoutUser();

        return new RedirectResponse('/tasks');
    }

    public function registerForm(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
        }

        $template = $this->view->load('users/register.html.twig');

        return new HtmlResponse($template->render());
    }

    public function register(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $this->userLogin();
        } catch (DomainException $e) {
        }

        $login = $_POST['login'] ?? null;
        $password = $_POST['password'] ?? null;

        Assertion::notEmpty($login);
        Assertion::notEmpty($password);

        if ($this->users->hasByLogin($login)) {
            Session::addError('User already exists');

            return new RedirectResponse('/register');
        }

        $user = User::create($login, md5($password));
        $this->users->add($user);
        $this->flusher->flush();
        $this->authUser($user);

        return new RedirectResponse('/tasks');
    }
}