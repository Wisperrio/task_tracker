<?php

return [
    'dbname' => 'task_tracker',
    'user' => 'postgres',
    'password' => 'postgres',
    'host' => '127.0.0.1',
    'driver' => 'pdo_pgsql',
];