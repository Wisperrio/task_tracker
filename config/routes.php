<?php

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

return simpleDispatcher(function (RouteCollector $router) {
    $router->get('/', ['App\Controller\TaskController', 'index']);
    $router->get('/tasks', ['App\Controller\TaskController', 'index']);
    $router->get('/tasks/create', ['App\Controller\TaskController', 'create']);
    $router->post('/tasks/create', ['App\Controller\TaskController', 'store']);
    $router->get('/tasks/{id}', ['App\Controller\TaskController', 'show']);
    $router->get('/tasks/{id}/edit', ['App\Controller\TaskController', 'edit']);
    $router->post('/tasks/{id}/edit', ['App\Controller\TaskController', 'update']);
    $router->get('/tasks/{id}/complete', ['App\Controller\TaskController', 'complete']);
    $router->get('/login', ['App\Controller\UserController', 'loginForm']);
    $router->post('/login', ['App\Controller\UserController', 'login']);
    $router->get('/logout', ['App\Controller\UserController', 'logout']);
    $router->get('/register', ['App\Controller\UserController', 'registerForm']);
    $router->post('/register', ['App\Controller\UserController', 'register']);
});