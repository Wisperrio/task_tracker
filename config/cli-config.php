<?php

require 'vendor/autoload.php';

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\EventManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Helper\HelperSet;

$paths = [__DIR__."\..\app\Entity"];
$isDevMode = true;

$settings = [
    'dev_mode' => false,
    'connection' => [
        'driver' => 'pdo_pgsql',
        'host' => '127.0.0.1',
        'user' => 'postgres',
        'password' => 'postgres',
        'dbname' => 'task_tracker',
        'charset' => 'utf-8'
    ],
    'subscribers' => [],
    'metadata_dirs' => [
        __DIR__ . "\..\app\Entity"
    ],
    'types' => [
        App\Entity\RoleType::NAME => App\Entity\RoleType::class,
        App\Entity\StatusType::NAME => App\Entity\StatusType::class,
    ],
];

$config = Setup::createAnnotationMetadataConfiguration(
    $settings['metadata_dirs'],
    $settings['dev_mode'],
);

$config->setNamingStrategy(new UnderscoreNamingStrategy());

foreach ($settings['types'] as $name => $class) {
    if (!Type::hasType($name)) {
        Type::addType($name, $class);
    }
}

$eventManager = new EventManager();

foreach ($settings['subscribers'] as $name) {
    /** @var EventSubscriber $subscriber */
    $subscriber = $container->get($name);
    $eventManager->addEventSubscriber($subscriber);
}

$entityManager = EntityManager::create(
    $settings['connection'],
    $config,
    $eventManager
);

return new HelperSet([
    'em' => new EntityManagerHelper($entityManager),
//    'db' => new ConnectionHelper($entityManager->getConnection()),
]);