<?php

use App\Controller\TaskController;
use App\Controller\UserController;
use App\Entity\Task;
use App\Entity\TaskRepository;
use App\Entity\User;
use App\Entity\UserRepository;
use App\FlashExtension;
use App\Flusher;
use App\Session;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\EventManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Tools\Setup;
use Pagerfanta\Twig\Extension\PagerfantaExtension;
use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\RuntimeLoader\ContainerRuntimeLoader;

return [
    //Doctrine
    EntityManagerInterface::class => function (ContainerInterface $container): EntityManagerInterface {
        /**
         * @psalm-suppress MixedArrayAccess
         * @psalm-var array{
         *     metadata_dirs:array,
         *     dev_mode:bool,
         *     proxy_dir:string,
         *     cache_dir:?string,
         *     types:array<string,string>,
         *     subscribers:string[],
         *     connection:array
         * } $settings
         */
        $settings = $container->get('config')['doctrine'];

        $config = Setup::createAnnotationMetadataConfiguration(
            $settings['metadata_dirs'],
            $settings['dev_mode'],
            $settings['proxy_dir'],
            $settings['cache_dir'] ? new FilesystemCache($settings['cache_dir']) : new ArrayCache(),
            false
        );

        $config->setNamingStrategy(new UnderscoreNamingStrategy());

        foreach ($settings['types'] as $name => $class) {
            if (!Type::hasType($name)) {
                Type::addType($name, $class);
            }
        }

        $eventManager = new EventManager();

        foreach ($settings['subscribers'] as $name) {
            /** @var EventSubscriber $subscriber */
            $subscriber = $container->get($name);
            $eventManager->addEventSubscriber($subscriber);
        }

        return EntityManager::create(
            $settings['connection'],
            $config,
            $eventManager
        );
    },

    'config' => [
        'doctrine' => [
            'dev_mode' => false,
            'connection' => [
                'driver' => 'pdo_pgsql',
                'host' => '127.0.0.1',
                'user' => 'postgres',
                'password' => 'postgres',
                'dbname' => 'task_tracker',
                'charset' => 'utf-8'
            ],
            'subscribers' => [],
            'metadata_dirs' => [
                __DIR__."\..\app\Entity"
            ],
            'types' => [
                App\Entity\RoleType::NAME => App\Entity\RoleType::class,
                App\Entity\StatusType::NAME => App\Entity\StatusType::class,
            ],
        ],
    ],
    Flusher::class => function (ContainerInterface $container): Flusher {
        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        return new Flusher($em);
    },
    Environment::class => function (ContainerInterface $container) {
        $refl = new ReflectionClass(PagerfantaExtension::class);
        $path = dirname($refl->getFileName(), 2) . '/templates';
        $loader = new FilesystemLoader(__DIR__ . '/../resources/templates');
        $loader->addPath($path, 'Pagerfanta');
        $environment = new Environment($loader);

        $environment->addRuntimeLoader(new ContainerRuntimeLoader($container));
        $environment->addExtension(new PagerfantaExtension());
        $environment->addExtension(new FlashExtension());
        $environment->addGlobal('session', Session::getInstance());
        $environment->addGlobal('cookie', $_COOKIE);
        $environment->addGlobal('get', $_GET);

        return $environment;
    },
    UserRepository::class => function (ContainerInterface $container): UserRepository {
        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        /** @var EntityRepository $repo */
        $repo = $em->getRepository(User::class);

        return new UserRepository($em, $repo);
    },
    UserController::class => function (ContainerInterface $container): UserController {
        /** @var UserRepository $repo */
        $repo = $container->get(UserRepository::class);
        /** @var Flusher $flusher */
        $flusher = $container->get(Flusher::class);
        /** @var Flusher $flusher */
        $view = $container->get(Environment::class);

        return new UserController($repo, $flusher, $view);
    },
    TaskRepository::class => function (ContainerInterface $container): TaskRepository {
        /** @var EntityManagerInterface $em */
        $em = $container->get(EntityManagerInterface::class);
        /** @var EntityRepository $repo */
        $repo = $em->getRepository(Task::class);

        return new TaskRepository($em, $repo);
    },
    TaskController::class => function (ContainerInterface $container): TaskController {
        /** @var TaskRepository $repo */
        $repo = $container->get(TaskRepository::class);
        /** @var Flusher $flusher */
        $flusher = $container->get(Flusher::class);
        /** @var Flusher $flusher */
        $view = $container->get(Environment::class);
        /** @var UserRepository $repo */
        $userRepo = $container->get(UserRepository::class);

        return new TaskController($repo, $flusher, $view, $userRepo);
    },
];